# Description

Sets the timezone.

# License

Released under the Apache License, Version 2.0.
