require 'spec_helper'

describe 'timezone' do

  context 'with defaults' do
    context 'debian' do
      let(:facts) { { osfamily: 'Debian' } }

      it {
        should contain_file('/etc/timezone').with(
          content: "Europe/London\n",
          owner:   'root',
          group:   'root',
          mode:    '0644'
        )
      }

      it { should contain_exec('/usr/sbin/dpkg-reconfigure -f noninteractive tzdata') }
    end

    context 'other' do
      let(:facts) { { osfamily: 'Darwin' } }

      it { expect { should compile }.to raise_error }
    end
  end

  context 'with params' do
    let(:params) { { timezone: 'Etc/UTC' } }

    context 'debian' do
      let(:facts) { { osfamily: 'Debian' } }

      it {
        should contain_file('/etc/timezone').with(
          content: "Etc/UTC\n",
          owner:   'root',
          group:   'root',
          mode:    '0644'
        )
      }

      it { should contain_exec('/usr/sbin/dpkg-reconfigure -f noninteractive tzdata') }
    end

    context 'other' do
      let(:facts) { { osfamily: 'Darwin' } }

      it { expect { should compile }.to raise_error }
    end
  end

end
